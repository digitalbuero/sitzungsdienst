"""
This module is part of the 'sitzungsdienst' package,
which is released under GPL-3.0-only license.
"""

import asyncio
import io
import json

import aiofiles
import pytest

from sitzungsdienst.models.court import CourtDate, CourtDates
from sitzungsdienst.models.express import ExpressDate, ExpressDates
from sitzungsdienst.sta import StA, format_date, format_place, is_court


def test_sta():
    """
    Tests 'sta.run' (no async)
    """

    # Run functions
    with open("tests/fixtures/test.pdf", "rb") as file:
        court_dates1, express_dates1 = asyncio.run(StA.run(file))
        court_dates2, express_dates2 = StA.runs(file)

    # Assert results
    assert isinstance(court_dates1, CourtDates)
    assert isinstance(court_dates2, CourtDates)
    assert isinstance(express_dates1, ExpressDates)
    assert isinstance(express_dates2, ExpressDates)

    for court_date in court_dates1:
        assert isinstance(court_date, CourtDate)

    for court_date in court_dates2:
        assert isinstance(court_date, CourtDate)

    for express_date in express_dates1:
        assert isinstance(express_date, ExpressDate)

    for express_date in express_dates2:
        assert isinstance(express_date, ExpressDate)


@pytest.mark.asyncio
async def test_sta_bytes():
    """
    Tests 'sta.run' (bytes input)
    """

    # Run function
    async with aiofiles.open("tests/fixtures/test.pdf", "rb") as file:
        court_dates, express_dates = await StA.run(await file.read())

    # Assert results
    assert isinstance(court_dates, CourtDates)
    assert isinstance(express_dates, ExpressDates)

    for court_date in court_dates:
        assert isinstance(court_date, CourtDate)

    for express_date in express_dates:
        assert isinstance(express_date, ExpressDate)


@pytest.mark.asyncio
async def test_run_single():
    """
    Tests 'sta.run' (single input)
    """

    # Setup
    # (1) Load test PDFs
    async with aiofiles.open("tests/fixtures/test.pdf", "rb") as file:
        court_dates_stream1, express_dates_stream1 = await StA.run(
            io.BytesIO(await file.read())
        )

    async with aiofiles.open("tests/fixtures/test2.pdf", "rb") as file:
        court_dates_stream2, express_dates_stream2 = await StA.run(
            io.BytesIO(await file.read())
        )

    async with aiofiles.open("tests/fixtures/test3.pdf", "rb") as file:
        court_dates_stream3, express_dates_stream3 = await StA.run(
            io.BytesIO(await file.read())
        )

    async with aiofiles.open("tests/fixtures/no-express.pdf", "rb") as file:
        court_dates_stream4, express_dates_stream4 = await StA.run(
            io.BytesIO(await file.read())
        )

    # (2) Load expected data
    async with aiofiles.open("tests/fixtures/test.json", "r") as file1, aiofiles.open(
        "tests/fixtures/test2.json", "r"
    ) as file2, aiofiles.open("tests/fixtures/test3.json", "r") as file3, aiofiles.open(
        "tests/fixtures/no-express.json", "r"
    ) as file4:
        expected1, expected2, expected3, expected4 = (
            json.loads(await file1.read()),
            json.loads(await file2.read()),
            json.loads(await file3.read()),
            json.loads(await file4.read()),
        )

    # Run function #1
    court_dates1, express_dates1 = await StA.run("tests/fixtures/test.pdf")

    # Assert result #1
    assert len(court_dates1) == len(court_dates_stream1) == 83
    assert court_dates1.to_dict() == court_dates_stream1.to_dict() == expected1
    assert (
        express_dates1.to_dict()
        == express_dates_stream1.to_dict()
        == [
            {
                "start": "2021-11-26",
                "end": "2021-11-29",
                "who": {
                    "title": "EOAA",
                    "doc": "",
                    "first": "Adalbert",
                    "last": "Plattner",
                    "department": "",
                },
                "others": [],
            },
            {
                "start": "2021-11-29",
                "end": "2021-12-03",
                "who": {
                    "title": "StA",
                    "doc": "",
                    "first": "Max",
                    "last": "Häberling",
                    "department": "",
                },
                "others": [],
            },
            {
                "start": "2021-12-03",
                "end": "2021-12-06",
                "who": {
                    "title": "StA",
                    "doc": "Dr.",
                    "first": "Julien",
                    "last": "Eisner",
                    "department": "",
                },
                "others": [],
            },
        ]
    )

    # Run function #2
    court_dates2, express_dates2 = await StA.run("tests/fixtures/test2.pdf")

    # Assert result #2
    assert len(court_dates2) == len(court_dates_stream2) == 103
    assert court_dates2.to_dict() == expected2
    assert (
        express_dates2.to_dict()
        == express_dates_stream2.to_dict()
        == [
            {
                "start": "2021-12-03",
                "end": "2021-12-06",
                "who": {
                    "title": "StA",
                    "doc": "Dr.",
                    "first": "Julien",
                    "last": "Eisner",
                    "department": "",
                },
                "others": [],
            },
            {
                "start": "2021-12-07",
                "end": "2021-12-07",
                "who": {
                    "title": "EStA",
                    "doc": "",
                    "first": "Kosta",
                    "last": "Beck",
                    "department": "",
                },
                "others": [],
            },
        ]
    )

    # Run function #3
    court_dates3, express_dates3 = await StA.run("tests/fixtures/test3.pdf")

    # Assert result #3
    assert len(court_dates3) == len(court_dates_stream3) == 75
    assert court_dates3.to_dict() == court_dates_stream3.to_dict() == expected3
    assert (
        express_dates3.to_dict()
        == express_dates_stream3.to_dict()
        == [
            {
                "start": "2021-11-19",
                "end": "2021-11-22",
                "who": {
                    "title": "StA'in",
                    "doc": "Dr.",
                    "first": "Cordula",
                    "last": "Zeitz",
                    "department": "",
                },
                "others": [],
            },
            {
                "start": "2021-11-22",
                "end": "2021-11-26",
                "who": {
                    "title": "StA",
                    "doc": "",
                    "first": "Kosta",
                    "last": "Alfkotte",
                    "department": "",
                },
                "others": [],
            },
            {
                "start": "2021-11-26",
                "end": "2021-11-29",
                "who": {
                    "title": "EOAA",
                    "doc": "",
                    "first": "Adalbert",
                    "last": "Plattner",
                    "department": "",
                },
                "others": [],
            },
        ]
    )

    # Run function #4
    court_dates4, express_dates4 = await StA.run("tests/fixtures/no-express.pdf")

    # Assert result #4
    assert len(court_dates4) == len(court_dates_stream4) == 29
    assert court_dates4.to_dict() == court_dates_stream4.to_dict() == expected4
    assert express_dates4.to_dict() == express_dates_stream4.to_dict() == []


@pytest.mark.asyncio
async def test_run_multiple():
    """
    Tests 'sta.run' (multiple inputs)
    """

    # Setup
    # (1) Expected court dates
    async with aiofiles.open(
        "tests/fixtures/all.json", "r", encoding="utf-8"
    ) as json_file:
        expected_court_dates = json.loads(await json_file.read())

    # (2) Expected express service dates
    expected_express_dates = [
        {
            "start": "2021-11-19",
            "end": "2021-11-22",
            "who": {
                "title": "StA'in",
                "doc": "Dr.",
                "first": "Cordula",
                "last": "Zeitz",
                "department": "",
            },
            "others": [],
        },
        {
            "start": "2021-11-22",
            "end": "2021-11-26",
            "who": {
                "title": "StA",
                "doc": "",
                "first": "Kosta",
                "last": "Alfkotte",
                "department": "",
            },
            "others": [],
        },
        {
            "start": "2021-11-26",
            "end": "2021-11-29",
            "who": {
                "title": "EOAA",
                "doc": "",
                "first": "Adalbert",
                "last": "Plattner",
                "department": "",
            },
            "others": [],
        },
        {
            "start": "2021-11-29",
            "end": "2021-12-03",
            "who": {
                "title": "StA",
                "doc": "",
                "first": "Max",
                "last": "Häberling",
                "department": "",
            },
            "others": [],
        },
        {
            "start": "2021-12-03",
            "end": "2021-12-06",
            "who": {
                "title": "StA",
                "doc": "Dr.",
                "first": "Julien",
                "last": "Eisner",
                "department": "",
            },
            "others": [],
        },
        {
            "start": "2021-12-07",
            "end": "2021-12-07",
            "who": {
                "title": "EStA",
                "doc": "",
                "first": "Kosta",
                "last": "Beck",
                "department": "",
            },
            "others": [],
        },
    ]

    # (3) Load test PDFs
    async with aiofiles.open("tests/fixtures/test.pdf", "rb") as file1, aiofiles.open(
        "tests/fixtures/test2.pdf", "rb"
    ) as file2, aiofiles.open("tests/fixtures/test3.pdf", "rb") as file3, aiofiles.open(
        "tests/fixtures/no-express.pdf", "rb"
    ) as file4:
        court_dates_stream, express_dates_stream = await StA.run(
            [
                io.BytesIO(await file1.read()),
                io.BytesIO(await file2.read()),
                io.BytesIO(await file3.read()),
                io.BytesIO(await file4.read()),
            ]
        )

    # Run function
    court_dates, express_dates = await StA.run(
        [
            "tests/fixtures/test.pdf",
            "tests/fixtures/test2.pdf",
            "tests/fixtures/test3.pdf",
            "tests/fixtures/no-express.pdf",
        ]
    )

    # Assert results
    assert len(court_dates) == len(court_dates_stream) == 290
    assert len(express_dates) == len(express_dates_stream) == 6
    assert court_dates.to_dict() == court_dates_stream.to_dict() == expected_court_dates
    assert (
        express_dates.to_dict()
        == express_dates_stream.to_dict()
        == expected_express_dates
    )


@pytest.mark.asyncio
async def test_filter():
    """
    Tests 'filter'
    """

    # Run function
    court_dates, express_dates = await StA.run("tests/fixtures/test.pdf")

    # Assert result #1
    assert court_dates.filter("Chloe Bähnle").to_dict() == [
        {
            "date": "2021-12-01",
            "what": "821 Js 46097/19",
            "when": "08:30",
            "where": "LG Freiburg im Breisgau - Strafkammer VI - III/2. OG",
            "who": {
                "title": "StA'in",
                "doc": "",
                "first": "Chloe",
                "last": "Bähnle",
                "department": "",
            },
            "others": [],
        },
        {
            "date": "2021-12-02",
            "what": "854 Js 36239/19",
            "when": "08:30",
            "where": "LG Freiburg im Breisgau - Strafkammer VI - III/2. OG",
            "who": {
                "department": "",
                "doc": "",
                "first": "Chloe",
                "last": "Bähnle",
                "title": "StA'in",
            },
            "others": [],
        },
    ]

    # Assert result #3
    assert express_dates.filter("Adalbert Plattner").to_dict() == [
        {
            "start": "2021-11-26",
            "end": "2021-11-29",
            "who": {
                "title": "EOAA",
                "doc": "",
                "first": "Adalbert",
                "last": "Plattner",
                "department": "",
            },
            "others": [],
        },
    ]

    # Assert result #4
    assert express_dates.filter(["EOAA", "julien"]).to_dict() == [
        {
            "start": "2021-11-26",
            "end": "2021-11-29",
            "who": {
                "title": "EOAA",
                "doc": "",
                "first": "Adalbert",
                "last": "Plattner",
                "department": "",
            },
            "others": [],
        },
        {
            "start": "2021-12-03",
            "end": "2021-12-06",
            "who": {
                "title": "StA",
                "doc": "Dr.",
                "first": "Julien",
                "last": "Eisner",
                "department": "",
            },
            "others": [],
        },
    ]


@pytest.mark.asyncio
async def test_filter_no_query():
    """
    Tests 'filter' (no query)
    """

    # Run function
    court_dates, express_dates = await StA.run("tests/fixtures/test.pdf")

    # Assert results
    assert (
        court_dates.to_dict()
        == court_dates.filter("").to_dict()
        == court_dates.filter([]).to_dict()
    )
    assert (
        express_dates.to_dict()
        == express_dates.filter("").to_dict()
        == express_dates.filter([]).to_dict()
    )


@pytest.mark.asyncio
async def test_court_dates_data2ics():
    """
    Tests 'sta.court_dates.data2ics'
    """

    # Setup
    # (1) Expected court date ICS data
    ics_data1 = [
        "BEGIN:VCALENDAR",
        "VERSION:2.0",
        "PRODID:S1SYPHOS",
        "BEGIN:VEVENT",
        "ATTENDEE;CN=StA'in Chloe Bähnle:mailto:",
        "DTSTAMP:20220523T202710Z",
        "DTEND:20211202T083000Z",
        "LOCATION:LG Freiburg im Breisgau - Strafkammer VI - III/2. OG",
        "DTSTART:20211202T073000Z",
        "SUMMARY:Sitzungsdienst (854 Js 36239/19)",
        "UID:SOMETOTALLYRANDOMSTRINGIDENTIFIER",
        "END:VEVENT",
        "BEGIN:VEVENT",
        "ATTENDEE;CN=StA'in Chloe Bähnle:mailto:",
        "DTSTAMP:20220523T202710Z",
        "DTEND:20211201T083000Z",
        "LOCATION:LG Freiburg im Breisgau - Strafkammer VI - III/2. OG",
        "DTSTART:20211201T073000Z",
        "SUMMARY:Sitzungsdienst (821 Js 46097/19)",
        "UID:SOMETOTALLYRANDOMSTRINGIDENTIFIER",
        "END:VEVENT",
        "END:VCALENDAR",
    ]

    # (2) Expected express service date ICS data
    ics_data2 = [
        "BEGIN:VCALENDAR",
        "VERSION:2.0",
        "PRODID:S1SYPHOS",
        "BEGIN:VEVENT",
        "DTSTART;VALUE=DATE:20211125",
        "DTEND;VALUE=DATE:20211129",
        "ATTENDEE;CN=EOAA Adalbert Plattner:mailto:",
        "DTSTAMP:20220523T202710Z",
        "SUMMARY:Eildienst (StA)",
        "UID:SOMETOTALLYRANDOMSTRINGIDENTIFIER",
        "END:VEVENT",
        "END:VCALENDAR",
    ]

    # (3) Create instances
    court_dates, express_dates = await StA.run("tests/fixtures/test.pdf")

    # Assert result #1
    assert len(court_dates.data2ics().events) == 83
    assert len(express_dates.data2ics().events) == 3

    # Run function #2
    result1 = court_dates.filter("Chloe Bähnle").data2ics()

    # Assert result #2
    assert len(result1.events) == 2

    for i, line in enumerate(result1.serialize().splitlines()):
        # Skip creation timestamp & unique identifier
        if "DTSTAMP" in line or "UID" in line:
            continue

        assert line == ics_data1[i]

    # Run function #3
    result2 = express_dates.filter("Adalbert Plattner").data2ics()

    # Assert result #3
    assert len(result2.events) == 1

    for i, line in enumerate(result2.serialize().splitlines()):
        # Skip creation timestamp & unique identifier
        if "DTSTAMP" in line or "UID" in line:
            continue

        assert line == ics_data2[i]


@pytest.mark.asyncio
async def test_court_dates_data2ics_duration():
    """
    Tests 'sta.court_dates.data2ics' (using duration)
    """

    # Setup
    # (1) Expected end times
    expected = ["DTEND:20211201T103000Z", "DTEND:20211202T103000Z"]

    # (2) Create instance
    court_dates, _ = await StA.run("tests/fixtures/test.pdf")

    # Run function
    result = court_dates.filter("Chloe Bähnle").data2ics(3).serialize()

    # Assert result
    for end_time in expected:
        assert end_time in result


@pytest.mark.asyncio
async def test_is_court():
    """
    Tests 'sta.is_court'
    """

    # Assert results
    assert is_court(" AG Sonstwo (Foyer) ")
    assert is_court("AG Nirgendwo - Abt. 1 - Zi. 3.01")
    assert is_court("LG Musterstadt, Saal IX - Musterweg 4")
    assert not is_court("BAG Landeshauptstadt, 3. Hinterzimmer")
    assert not is_court("AGA AGA")


@pytest.mark.asyncio
async def test_format_date():
    """
    Tests 'sta.format_date'
    """

    # Assert results
    assert "2022-01-01" == format_date("01.01.2022")


@pytest.mark.asyncio
async def test_format_place():
    """
    Tests 'sta.format_place'
    """

    # Assert results
    assert "AG Irgendwo - Abt. 6 -" == format_place("AG Irgendwo , - Abt. 6 -", "")
    assert "LG Musterstadt - 1. OG" == format_place("LG Musterstadt , -", "1. OG ")
